% Simple BCI: 8-26Hz bandpass filtering/ CSP filter/ log (var)/LDA
clc
clear all
close all

%%%%%%% loading data
load('../data/trar.mat')
fs=125        % Sampling Frequency
Left1=tr{1};   % 6x875x20
Right1=tr{2};  % 6x875x20

%%%%%% Spectral Filtering (band pass filteing in 8-26 Hz using butterworth filter)
Left=zeros(size(tr{1},1)-3,size(tr{1},2),size(tr{1},3));
Right=zeros(size(tr{2},1)-3,size(tr{2},2),size(tr{2},3));
[B,A] = butter(5,[8 26]/fs*2);
for channel=1:3
    for trial=1:size(Left1,3)
        
        Right(channel,:,trial)= filter(B,A,Right1(channel,:,trial));
        Left(channel,:,trial)= filter(B,A,Left1(channel,:,trial));
    end
end

ii=0;
for t=250:1:875,% for each time sample we calculate the accuracy to determine the best point for detecting right versus left hand movement in the test phase
    ii=ii+1;
    trial=ones(size(tr{1},3),1);
    indices = crossvalind('Kfold',trial,5);
    %indices = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5 ];
    AC=[];
    for i=1:5 % five fold cross validation
        
        train=find(indices~=i);        
        test=find(indices==i);      
        trainRight=Right(:,:,train);        
        trainLeft=Left(:,:,train);        
        RI=zeros(size(tr{1},1)-3,size(tr{1},1)-3);
        
        for j=1:size(trainRight,3)
            Trial=trainRight(:,t-249:t,j)';
            RI=RI+Trial'*Trial/trace(Trial'*Trial);
        end
        RI=RI./size(trainRight,3);
        
        RJ=zeros(size(tr{1},1)-3,size(tr{1},1)-3);
        for j=1:size(trainLeft,3)
            Trial=trainLeft(:,t-249:t,j)';
            RJ=RJ+Trial'*Trial/trace(Trial'*Trial);
        end
        RJ=RJ./size(Left,3);
        RC=RI+RJ;
        [V,D]=eig(RC);
    end   
    
end
RC



