% extracting data files from output text file of C# program
clc
clear all
close all
load('C:\Users\P1028\Desktop\work\Dr Coyle matlab Code\data\trar.mat');

% saving all trials in one time series in txt file for c sharp
s1=tr{1};
s2=tr{2};
A=[];
A1=[];
for i=1:20
    A=[A,s1(1:3,:,i)];
    A1=[A1,s2(1:3,:,i)];
end
A=A';
A1=A1';
content=who;
save('C:\Users\P1028\Desktop\work\Dr Coyle matlab Code\data\Left.txt',content{1},'-ascii')
save('C:\Users\P1028\Desktop\work\Dr Coyle matlab Code\data\Right.txt',content{2},'-ascii')

% filtering in frequency band [8-26] hz
fs=125;
[b,a] = butter(5,[8 26]/fs*2);
Afilt=[];
A1filt=[];
for i=1:3
    Afilt(:,i)=filter(b,a,A(:,i));
    A1filt(:,i)=filter(b,a,A1(:,i));
end

plot(Afilt(:,1))
figure
plot(A1filt(:,1))
% comparision between the saved results of C# program with Matlab results
% (leftfilt and rightfilt are two .txt files which created in C# for saving
% the filtered signal of each class

AfiltC=importdata('C:\Users\P1028\Desktop\work\Dr Coyle matlab Code\data\leftfilt.txt');
AfiltC=AfiltC';
A1filtC=importdata('C:\Users\P1028\Desktop\work\Dr Coyle matlab Code\data\rightfilt.txt');
A1filtC=A1filtC';

g=Afilt-AfiltC;
g1=A1filt-A1filtC;


k=find(g~=0);
k1=find(g1~=0);

