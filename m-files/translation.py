# PYTHON  

# Libraries
from numpy import *
from scipy import optimize
from scipy import interpolate
from scipy import integrate
from scipy import signal
from scipy.signal import lfilter
import scipy.linalg
import matplotlib.pyplot as plt
from pylab import *

import numpy as np
import scipy.io as io
import matplotlib
import mat4py
from random import *
import random




# Start -----------------------------------------------------------------

matfile = io.loadmat('../data/trar.mat', chars_as_strings=1 ) # Load trar.mat
tr = np.array(matfile['tr']) # transform in array
fs = 125
Left1=tr[0][0]
Right1=tr[0][1]
Left=zeros(( (tr[0][0].shape[0])-3, (tr[0][0].shape[1]), (tr[0][0].shape[2]) ))
Right=zeros(( (tr[0][1].shape[0])-3, (tr[0][1].shape[1]), (tr[0][1].shape[2]) ))

A = [1, -5.17974472959823, 13.0570630628250, -21.1249179224401, 24.3133376016936, -20.7293252005955, 13.2123640685592, -6.21014485260958, 2.06633483995875, -0.441880374629354, 0.0468518433155365]
B = [0.00587121942215584, 0, -0.0293560971107792, 0, 0.0587121942215584, 0, -0.0587121942215584, 0, 0.0293560971107792, 0, -0.00587121942215584]

channel = 1
trial = 1

for channel in range(3):
	for trial in range(Left1.shape[2]):
		Right[channel,:,trial]=signal.lfilter(B,A,Right1[channel,:,trial])
		Left[channel,:,trial]=signal.lfilter(B,A,Left1[channel,:,trial])

ii=0
t=250
rng5=range (1,5)
rng4=range (0,4)
rng3=range(0,3)
rng250=range(0,250)
rng250876=range(250,876)

for t in rng250876:
	ii=ii+1		
	AC=[]	
	RI=zeros(( (tr[0][0].shape[0])-3, (tr[0][0].shape[0]-3) ))
	RJ=zeros(( (tr[0][0].shape[0])-3, (tr[0][0].shape[0]-3) ))
	
	for i in rng5:
		#train=find(indices~=i);---------------------        
        #test=find(indices==i);------------------------
		k=20
		test=[0, 0, 0, 0]
		indices=[0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4] #no random
		train=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
		shuffle(indices) #melange function by hand, more efficient	
		#matlab function find by hand
		indexMatLabFunctionFind=0
		for indexMatLabFunctionFind in rng4: 		
			j=random.randint(1,k-1)
			test[indexMatLabFunctionFind]=train[j]			
			#indices[j]=0							
			del train[j]
			k=k-1
		test.sort()
		
		#trainRight=Right(:,:,train);-----------------------------		
		trainRight4=Right 		
		trainRight3=np.delete(trainRight4,test[3],axis=2)		
		trainRight2=np.delete(trainRight3,test[2],axis=2)		
		trainRight1=np.delete(trainRight2,test[1],axis=2)		
		trainRight=np.delete(trainRight1,test[0],axis=2)

		#trainLeft=Left(:,:,train);--------------------------------
		trainLeft4=Left 		
		trainLeft3=np.delete(trainLeft4,test[3],axis=2)		
		trainLeft2=np.delete(trainLeft3,test[2],axis=2)		
		trainLeft1=np.delete(trainLeft2,test[1],axis=2)		
		trainLeft=np.delete(trainLeft1,test[0],axis=2)					

		
		Trial2=[[0.]*250]*3
		Trialbis=np.array(Trial2)	

		#RI--------------------------------------------------------
		trainRightLen2=trainRight.shape[2]
		rngTrainRightLen2=range(0,trainRightLen2)
		for var in rngTrainRightLen2:
			for l in rng3:
				for c in rng250:
					Trialbis[l][c]=trainRight[l][t-250+c][var]
			Trial=Trialbis.T
			multTrial2=[[0.]*3]*3
			multTrial=np.array(multTrial2)
			#calcul matrice multTrial
			for x in rng3:
				for y in rng3:
					for z in rng250:
						multTrial[x][y] = multTrial[x][y] + np.multiply(Trialbis[x][z],Trial[z][y])			
			RI=RI+multTrial/np.trace(multTrial) #good operators
		RI=RI/trainRight.shape[2]

		#RJ----------------------------------------------------------		
			
		for var in rngTrainRightLen2:
			for l in rng3:
				for c in rng250:
					Trialbis[l][c]=trainLeft[l][t-250+c][var]
			Trial=Trialbis.T
			multTrial2=[[0.]*3]*3
			multTrial=np.array(multTrial2)
			#calcul matrice multTrial
			for x in rng3:
				for y in rng3:
					for z in rng250:
						multTrial[x][y] = multTrial[x][y] + np.multiply(Trialbis[x][z],Trial[z][y])			
			RJ=RJ+multTrial/np.trace(multTrial) #good operators
		RJ=RJ/trainRight.shape[2]
		RC2=RI+RJ

		#[V,D]=eig(RC)---------------------------------------------------
		RC=np.array(RC2)
		[D3,V]=linalg.eig(RC)
		D2=[[0.]*3 for _ in rng3]
		for i in rng3:
			D2[i][i]=D3[2-i]
		D=np.array(D2)
		for k in rng3:
			aux=V[k][0]
			V[k][0]=V[k][2]
			V[k][2]=-aux
			V[k][1]=-V[k][1]

# End --------------------------------------------------------



######## Display ########--------


print 'A : ', shape(A)
#print 'A : ', A
print 'AC : ', shape(AC)
#print 'AC : ', AC
print 'B : ', shape(B)
#print 'B : ', B
print 'channel : ', channel
print 'D :', D
print 'fs : ', fs
print 'ii : ', ii
print 'indices', shape(indices)
print indices
print 'Left : ', shape(Left)
#print Left
print 'Left1 : ', shape(Left1) 
#print Left1[5][874][19]
print 'RC : ', RC
print 'RI : ', RI
print 'RJ : ', RJ
print 'Right : ', shape(Right) 
print 'Right1 : ', shape(Right1) 
#print Right1[5][874][19]
print 't : ', t
print 'test', test
print 'tr : ', shape(tr) 
print 'train', shape(train)
print 'train', train
print 'trainRight', shape(trainRight)
print 'trial :', shape(trial)
#print 'trial :', trial
print 'Trial : ', shape(Trial)
print 'Trial : ', type(Trial)
print 'multTrial : ', multTrial
print 'trace : ', np.trace(multTrial)
print 'V : ', V

#------------------------------------------------------



######## Value Storage in file.txt #############


##########################Left1################################
#---------------------------------------------------------------

#fichier = open("/verify_data_2/Left1ForMatlab.txt", "w")
#for i in range(0,5):
#	for j in range (0,874):	
#		for k in range (0,19):
#			fichier.write(str(round(Left1ForMatlab[i][j][k],3)))
#			fichier.write(',')
#fichier.close() 

##To compare with matlab
fichier = open("verify_data_2/Left1ForMatlab.txt", "w")
for i in range(0,20):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i+1))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,6): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(Left1[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,6): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(Left1[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

##To compare with Python
fichier = open("verify_data_2/Left1Python.txt", "w")
for i in range(0,20):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,6): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(Left1[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,6): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(Left1[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()


#Right1-------------------------------------------
#fichier = open("verify_data_2/Right1Python.txt", "w")
#for i in range(0,5):
#	for j in range (0,874):	
#		for k in range (0,19):
#			fichier.write(str(round(Right1[i][j][k],3)))
#			fichier.write(',')
#fichier.close() 

##To compare with matlab
fichier = open("verify_data_2/Right1ForMatlab.txt", "w")
for i in range(0,20):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i+1))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,6): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(Right1[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,6): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(Right1[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

##To compare with Python
fichier = open("verify_data_2/Right1Python.txt", "w")
for i in range(0,20):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,6): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(Right1[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,6): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(Right1[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

#Left------------------------------------------
#fichier = open("verify_data_2/LeftPython.txt", "w")
#for i in range(0,2):
#	for j in range (0,874):	
#		for k in range (0,19):
#			fichier.write(str(round(Left[i][j][k],3)))
#			fichier.write(',')
#fichier.close() 

##To compare with matlab
fichier = open("verify_data_2/LeftForMatlab.txt", "w")
for i in range(0,20):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i+1))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(Left[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(Left[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

##To compare with Python
fichier = open("verify_data_2/LeftPython.txt", "w")
for i in range(0,20):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(Left[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(Left[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

#Right------------------------------------------
#fichier = open("verify_data_2/RightPython.txt", "w")
#for i in range(0,2):
#	for j in range (0,874):	
#		for k in range (0,19):
#			fichier.write(str(round(Right[i][j][k],3)))
#			fichier.write(',')
#fichier.close() 

##To compare with matlab
fichier = open("verify_data_2/RightForMatlab.txt", "w")
for i in range(0,20):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i+1))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(Right[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(Right[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

##To compare with Python
fichier = open("verify_data_2/RightPython.txt", "w")
for i in range(0,20):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(Right[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(Right[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

#trainRight-------------------------------------
#fichier = open("verify_data_2/trainRightPython.txt", "w")
#for i in range(0,2):
#	fichier.write ('[')
#	for j in range (0,874):
#		fichier.write('\n')	
#		for k in range (0,15):
#			fichier.write(str(round(trainRight[i][j][k],3)))
#			fichier.write(',')
#	fichier.write(']')

##To compare with matlab
fichier = open("verify_data_2/trainRightForMatlab.txt", "w")
for i in range(0,16):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i+1))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(trainRight[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(trainRight[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

##To compare with Python
fichier = open("verify_data_2/trainRightPython.txt", "w")
for i in range(0,16):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(trainRight[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(trainRight[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

#trainLeft-------------------------------------
#fichier = open("verify_data_2/trainLeftPython.txt", "w")
#for i in range(0,2):
#	fichier.write ('[')
#	for j in range (0,874):
#		fichier.write('\n')	
#		for k in range (0,15):
#			fichier.write(str(round(trainRight[i][j][k],3)))
#			fichier.write(',')
#	fichier.write(']')

##To compare with matlab
fichier = open("verify_data_2/trainLeftForMatlab.txt", "w")
for i in range(0,16):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i+1))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(trainLeft[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(trainLeft[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

##To compare with Python
fichier = open("verify_data_2/trainLeftPython.txt", "w")
for i in range(0,16):
	fichier.write('\n')
	fichier.write('val(:,:,')
	fichier.write(str(i))
	fichier.write(') = ')
	fichier.write('\n')
	c1=1
	c2=4
	for j in range(0,218): #manque une colonne
		fichier.write('\n')
		fichier.write('\n')
		fichier.write('Columns ')
		fichier.write(str(c1))
		fichier.write(' through ')
		fichier.write(str(c2))
		fichier.write('\n')		
		for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,4):
				fichier.write((str(round(trainLeft[k][c1+l-1][i],4))))
				fichier.write('    ')
		c1=c1+4
		c2=c2+4
	c2=c2-1
	fichier.write('\n')
	fichier.write('\n')
	fichier.write('Columns ')
	fichier.write(str(c1))
	fichier.write(' through ')
	fichier.write(str(c2))
	fichier.write('\n')
	for k in range(0,3): 
			fichier.write('\n')
			for l in range(0,3):
				fichier.write((str(round(trainLeft[k][c1+l-1][i],4))))
				fichier.write('    ')
	fichier.write('\n')		
fichier.close()

#Trial-------------------------------------
fichier = open("verify_data_2/TrialPython.txt", "w")
for i in range(0,250):	
	fichier.write(str(i))	
	fichier.write(' : ')
	for j in range (0,3):				
		fichier.write(str(round(Trial[i][j],4)))
		fichier.write('   ,   ')
	fichier.write('\n')



#-------------------------------------------------

###### Try ##########-----------------------------
#c = (a==matfile)
#d = (type(a)==type(matfile))
#print c
#print d
#print type(matfile)
#print type(a)
# print a
# print matfile
#e = -25.07597401 in matfile
#print e
#print matfile.keys()
#i = '__version__' in matfile
#print i
#f = np.array(matfile['tr'])
#print f[0][2]
#print Right[2][19][19] //derniere valeur
#print 'Test : ', Right[:,:,train[indexRight]]




