% In the name of God
% online phase (detecting right versus left hand movement)
clc
clear all
close all 
%% loading data and save them in a way to simulate online system(all trials of each class are saved in one time series)
load('../data/trar.mat')
s1=tr{1};
s2=tr{2};
A1=[];
A2=[];
for i=1:20
    A1=[A1,s1(1:3,:,i)];
    A2=[A2,s2(1:3,:,i)];
end
AA=[A1 A2];
Label=ones(1,size(AA,2));

%% LDA parameters and CSP projection matrix are given here as input (after running training m-file)
load('coefficients.mat'); % P1 projection matrix loaded after running training m-file
fs=125;
[B,A] = butter(5,[8 26]/fs*2); %% butterworth filter coefficients

%% continious classification of whole data 
s=zeros(3,250);
score=[];

for t=2*fs:size(AA,2) %% starting point is 2nd second of the signal (first two seconds are buffered)
   % filtering first 2 second of signal in frequency band [8-26 Hz]
    for channel=1:3
     s(channel,:) = filter(B,A,AA(channel,t-249:t));
    end
   % CSP PROJECTION
    Z1=P1*s;
   % log var feature extraction 
    feature=[[log([var(Z1(1,:)),var(Z1(2,:)),var(Z1(3,:))])]];
   % LDA Classification 
    score(i) = a0 + a1N'* feature';
    i=i+1
end
dlmwrite('verify_data/score.txt',score,'delimiter','\n','precision',5);




    
    