% Simple BCI: 8-26Hz bandpass filtering/ CSP filter/ log (var)/LDA
%clc
%clear all
%close all

%%%%%%% loading data
load('../data/trar.mat')
fs=125        % Sampling Frequency
Left1=tr{1};   % 6x875x20
Right1=tr{2};  % 6x875x20

%%%%%% Spectral Filtering (band pass filteing in 8-26 Hz using butterworth filter)
Left=zeros(size(tr{1},1)-3,size(tr{1},2),size(tr{1},3));
Right=zeros(size(tr{2},1)-3,size(tr{2},2),size(tr{2},3));
[B,A] = butter(5,[8 26]/fs*2);
for channel=1:3
    for trial=1:size(Left1,3)
        
        Right(channel,:,trial)= filter(B,A,Right1(channel,:,trial));
        Left(channel,:,trial)= filter(B,A,Left1(channel,:,trial));
    end
end

%%%%%% Spatial Filtering
%%

ii=0;
for t=250:1:875,% for each time sample we calculate the accuracy to determine the best point for detecting right versus left hand movement in the test phase
    ii=ii+1;
    trial=ones(size(tr{1},3),1);
    indices = crossvalind('Kfold',trial,5);
    %indices = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5 ];
    AC=[];
    for i=1:5 % five fold cross validation
        
        train=find(indices~=i);
        test=find(indices==i);
        trainRight=Right(:,:,train);
        trainLeft=Left(:,:,train);
        % CSP filtering calculations for finding the projection Matrix
        RI=zeros(size(tr{1},1)-3,size(tr{1},1)-3);
        for j=1:size(trainRight,3)
            Trial=trainRight(:,t-249:t,j)';
            RI=RI+Trial'*Trial/trace(Trial'*Trial);
        end
        RI=RI./size(trainRight,3);
        
        RJ=zeros(size(tr{1},1)-3,size(tr{1},1)-3);
        for j=1:size(trainLeft,3)
            Trial=trainLeft(:,t-249:t,j)';
            RJ=RJ+Trial'*Trial/trace(Trial'*Trial);
        end
        RJ=RJ./size(Left,3);
        RC=RI+RJ;
        [V,D]=eig(RC);
        W=D^(-0.5)*V';
        Sa=W*RI*W';
        [U,Q]=eig(Sa);
        P1=U'*W; % Projection Matrix P1
        
        %%%%%% Log var Feature Extraction
        trainfeaturel=[];
        for k=1:size(trainLeft,3)
            Trial=trainLeft(:,t-249:t,k)';
            Z1=P1*Trial';
            trainfeaturel=[trainfeaturel;[log([var(Z1(1,:)),var(Z1(2,:)),var(Z1(3,:))])]];%,var(Z1(4,:)),var(Z1(5,:)),var(Z1(6,:))])]];
        end
        trainfeatureR=[];
        for k=1:size(trainRight,3)
            Trial=trainRight(:,t-249:t,k)';
            Z1=P1*Trial';
            trainfeatureR=[trainfeatureR;[log([var(Z1(1,:)),var(Z1(2,:)),var(Z1(3,:))])]];%,var(Z1(4,:)),var(Z1(5,:)),var(Z1(6,:))])]];
        end
        
        %%%%%%% LDA Classifier parameters
        data1=trainfeaturel;
        X1=data1;
        mu1=mean(X1);
        
        data2=trainfeatureR;
        X2=data2;
        mu2=mean(X2);
        
        mu=(mu1+mu2)./2;
        sigma1=cov(data1);
        sigma2=cov(data2);
        sigma = (sigma1 + sigma2)/2;
        sigmaInv = inv(sigma);
        a0 = - (1/2) * (mu1 + mu2) * sigmaInv * (mu1 - mu2)';
        a1N = sigmaInv * (mu1 - mu2)';
        
        %%%%%%%testing phase
        test_label=[ones(1,size(test,1)),-1*ones(1,size(test,1))];
        testRight=Right(:,:,test);
        testLeft=Left(:,:,test);
        feature_test=[];
        D1=[];
        
        for j=1:size(test,1)
            Trial= testLeft(:,t-249:t,j)';
            Z1=P1*Trial';
            feature_test=[feature_test;log([var(Z1(1,:)),var(Z1(2,:)),var(Z1(3,:))])];
        end
        for j=1:size(test,1)
            Trial=testRight(:,t-249:t,j)';
            Z1=P1*Trial';
            feature_test=[feature_test;log([var(Z1(1,:)),var(Z1(2,:)),var(Z1(3,:))])];
        end
        
        nbData = size(feature_test,1); % number of all the test trials
        nbFeaturesPlus1 = size(feature_test,2); % (size o feature vector) number of features for each trial
        result.output = zeros(nbData,1);
        result.classes = zeros(nbData,1);
        
        %classifying the input data
        for i=1:size(feature_test,1)
            inputVec = feature_test(i,1:(nbFeaturesPlus1))';
            score = a0 + a1N' * inputVec;
            if score >= 0
                result.classes(i) = 1;
            else
                result.classes(i) = -1;
            end
            result.output(i) = score;
        end
        
        %computing the classification accuracy for each fold
        nbErrors = sum(test_label~=result.classes');
        AC = [AC,((nbData - nbErrors)/nbData) * 100];
        
    end
    
    l(ii)=mean(AC);
    
end

% plotting the mean accuracy versus trial samples
a=[1:7*125];
plot(a(250:1:end)./125,l)

[maxAC,besttimesample]=max(l);
besttime=(besttimesample+250); % the best time sample in each trial (for the test (online) phase is used)

%% use the best time point for determining the projection matrix/ LDA parameters for the online phase

t=besttime;

RI=zeros(size(tr{2},1)-3,size(tr{2},1)-3);
for j=1:size(tr{2},3)
    Trial=Right(:,t-249:t,j)';
    RI=RI+Trial'*Trial/trace(Trial'*Trial);
end
RI=RI./size(trainRight,3);

RJ=zeros(size(tr{1},1)-3,size(tr{1},1)-3);
for j=1:size(tr{1},3)
    Trial=Left(:,t-249:t,j)';
    RJ=RJ+Trial'*Trial/trace(Trial'*Trial);
end
RJ=RJ./size(Left,3);
RC=RI+RJ;
[V,D]=eig(RC);
W=D^(-0.5)*V';
Sa=W*RI*W';
[U,Q]=eig(Sa);
P1=U'*W; % Selected Projection Matrix P1 for online 

%%%%%% Log var Feature Extraction
trainfeaturel=[];
for k=1:size(tr{1},3)
    Trial=Left(:,t-249:t,k)';
    Z1=P1*Trial';
    trainfeaturel=[trainfeaturel;[log([var(Z1(1,:)),var(Z1(2,:)),var(Z1(3,:))])]];%,var(Z1(4,:)),var(Z1(5,:)),var(Z1(6,:))])]];
end
trainfeatureR=[];
for k=1:size(tr{2},3)
    Trial=Right(:,t-249:t,k)';
    Z1=P1*Trial';
    trainfeatureR=[trainfeatureR;[log([var(Z1(1,:)),var(Z1(2,:)),var(Z1(3,:))])]];%,var(Z1(4,:)),var(Z1(5,:)),var(Z1(6,:))])]];
end

%%%%%%% LDA Classifier parameters
data1=trainfeaturel;
X1=data1;
mu1=mean(X1);

data2=trainfeatureR;
X2=data2;
mu2=mean(X2);

mu=(mu1+mu2)./2;
sigma1=cov(data1);
sigma2=cov(data2);
sigma = (sigma1 + sigma2)/2;
sigmaInv = inv(sigma);
a0 = - (1/2) * (mu1 + mu2) * sigmaInv * (mu1 - mu2)'; % LDA coefficient for online phase
a1N = sigmaInv * (mu1 - mu2)'; % LDA coefficient for online phase
dlmwrite('verify_data/a0.txt',a0,'delimiter','\t','precision',5);
dlmwrite('verify_data/a1N.txt',a1N,'delimiter','\t','precision',5);
dlmwrite('verify_data/P1.txt',P1,'delimiter','\t','precision',5);
savefile = 'coefficients.mat';
save(savefile, 'a0', 'a1N','P1');




